from django.shortcuts import render
# Create your views here.
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
#from rest_framework.decorators import api_view
from restaurants.serializers import RestaurantSerializer
from rest_framework.response import Response
from rest_framework import status
from restaurants.models import Restaurant


class RestaurantAPIView(APIView):
    '''
        get:
            Return all restaurants.

        post:
            Create a new restaurant.

        delete:
            Remove an existing restaurant (only if exist param ID).

        patch:
            Update one or more fields on an existing restaurant (only if exist param ID).
    '''
    permission_classes = (AllowAny,)
    serializer_class = RestaurantSerializer

    def post(self, request):
        restaurant = request.data
        serializer = RestaurantSerializer(data=restaurant)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({"success": True}, status=status.HTTP_201_CREATED)

    def get(self, request, *args, **kwargs):
        primary_key = self.kwargs.get('pk')
        exist = Restaurant.objects.filter(id=primary_key).exists()
        if primary_key is None:
            all_restaurants = Restaurant.objects.all()
            serializer = RestaurantSerializer(all_restaurants, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)

        # only if exist this param, execute query
        if primary_key is not None and exist:
            find_restaurant = Restaurant.objects.get(id=primary_key)
            serializer = self.serializer_class(find_restaurant)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response({"success": False}, status=status.HTTP_412_PRECONDITION_FAILED)

    def patch(self, request, *args, **kwargs):
        # new data
        get_data = request.data
        # id of restaurant
        primary_key = self.kwargs.get('pk')
        exist = Restaurant.objects.filter(id=primary_key).exists()

        if primary_key is not None and exist:
            find_restaurant = Restaurant.objects.get(id=primary_key)
            # save only the items sent
            serializer = RestaurantSerializer(
                find_restaurant,
                data=get_data,
                partial=True
            )
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response({"success": True}, status=status.HTTP_200_OK)

        return Response({"success": False}, status=status.HTTP_412_PRECONDITION_FAILED)

    def delete(self, request, *args, **kwargs):
        # id of restaurant
        primary_key = self.kwargs.get('pk')
        exist = Restaurant.objects.filter(id=primary_key).exists()

        if primary_key is not None and exist:
            find_restaurant = Restaurant.objects.get(id=primary_key)
            find_restaurant.delete()
            return Response({"success": True}, status=status.HTTP_200_OK)

        return Response({"success": False}, status=status.HTTP_412_PRECONDITION_FAILED)


class StatisticAPIView(APIView):
    '''
        Provide you statistic endpoint
    '''
    permission_classes = (AllowAny,)
    serializer_class = RestaurantSerializer

    def get(self, request, *args, **kwargs):
        # TODO: make queries for filter by lat, lng & R
        data = {
            'count': 0,
            'avg': 0,
            'std': 0
        }
        return Response(data, status=status.HTTP_200_OK)
