from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.


class Restaurant(models.Model):
    '''
     Class: Restaurant definition
    '''

    rating = models.IntegerField(
        validators=[MaxValueValidator(4), MinValueValidator(0)],
        null=True
    )
    name = models.CharField(max_length=50, null=True)
    site = models.URLField(null=True)
    email = models.CharField(max_length=80, unique=True, null=True)
    phone = models.CharField(max_length=20, null=True)
    street = models.CharField(max_length=50, null=True)
    city = models.CharField(max_length=50, null=True)
    state = models.CharField(max_length=50, null=True)
    lat = models.FloatField(null=True)
    lng = models.FloatField(null=True)
