from django.conf.urls import url

from restaurants.views import RestaurantAPIView, StatisticAPIView

urlpatterns = [
    url(
        r'^$',
        RestaurantAPIView.as_view()
    ),
    url(
        r'^(?P<pk>\d+)/?$',
        RestaurantAPIView.as_view()
    ),
    url(
        r'^statistics/(?P<latitude>\d+)/(?P<longitude>\d+)/(?P<radius>\d+)/$', StatisticAPIView.as_view()
    )
]
